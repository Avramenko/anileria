<?php $this->beginContent('//layouts/base'); ?>
<!--=== Content Part  ===-->
    <?php echo Chtml::tag('div',['class'=>'container content'])?>
        <?php echo Chtml::tag('div',['class'=>'row'])?>
            <!-- Begin Sidebar Menu -->
            <?php echo Chtml::tag('div',['class'=>'col-md-3'])?>
                <?php if (Yii::app()->hasModule('blog')): ?>
                    <?php Yii::import('application.modules.blog.BlogModule');?>

                        <?= CHtml::link(
                            "<i class='glyphicon glyphicon-pencil'></i> " . Yii::t('BlogModule.blog', 'Add a post'),
                            ['/blog/publisher/write'],
                            ['class' => 'btn btn-success', 'style' => 'width: 100%;']);
                        ?>

                <?php endif; ?>
            <?php echo Chtml::closeTag('div')?>
            <!-- End Sidebar Menu -->
            <!-- Begin Content -->
            <?php echo Chtml::tag('div',['class'=>'col-md-9'])?>
                <?php echo $content; ?>
            <?php echo Chtml::closeTag('div')?>
            <!-- End Content -->


        <?php echo Chtml::closeTag('div')?>
    <?php echo Chtml::closeTag('div')?>
<!--/container-->
<!--=== End Content Part  ===-->
<?php $this->endContent(); ?>
