<!DOCTYPE html>
<!--[if IE 8]> <html lang="<?php echo Yii::app()->language; ?>" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="<?php echo Yii::app()->language; ?>" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="<?php echo Yii::app()->language; ?>"> <!--<![endif]-->
<head>
    <?php Yii::app()->controller->widget(
        'vendor.chemezov.yii-seo.widgets.SeoHead',
        [
            'httpEquivs'         => array(
                'Content-Type'     => 'text/html; charset=utf-8',
                'X-UA-Compatible'  => 'IE=edge,chrome=1',
                'Content-Language' => Yii::app()->language
            ),
            'defaultTitle'       => Yii::app()->getModule('yupe')->siteName,
            'defaultDescription' => Yii::app()->getModule('yupe')->siteDescription,
            'defaultKeywords'    => Yii::app()->getModule('yupe')->siteKeyWords,
        ]
    ); ?>

    <!-- Web Fonts -->
    <link rel="shortcut" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600&subset=cyrillic,latin">

    <?php
    $mainAssets = Yii::app()->getTheme()->getAssetsUrl();

    //Yii::app()->getClientScript()->registerCssFile($mainAssets . '/css/main.css');
    // yupe css styles
    Yii::app()->getClientScript()->registerCssFile($mainAssets . '/css/flags.css');
    Yii::app()->getClientScript()->registerCssFile($mainAssets . '/css/yupe.css');
    // end yupe css styles

    Yii::app()->getClientScript()->registerCssFile($mainAssets . '/plugins/bootstrap/css/bootstrap.min.css');
    Yii::app()->getClientScript()->registerCssFile($mainAssets . '/css/style.css');
    Yii::app()->getClientScript()->registerCssFile($mainAssets . '/css/headers/header-default.css');
    Yii::app()->getClientScript()->registerCssFile($mainAssets . '/css/footers/footer-v1.css');
    Yii::app()->getClientScript()->registerCssFile($mainAssets . '/plugins/animate.css');
    Yii::app()->getClientScript()->registerCssFile($mainAssets . '/plugins/line-icons/line-icons.css');
    Yii::app()->getClientScript()->registerCssFile($mainAssets . '/plugins/font-awesome/css/font-awesome.css');
    Yii::app()->getClientScript()->registerCssFile($mainAssets . '/css/theme-colors/orange.css');
    Yii::app()->getClientScript()->registerCssFile($mainAssets . '/plugins/owl-carousel/owl-carousel/owl.carousel.css');
    Yii::app()->getClientScript()->registerCssFile($mainAssets . '/plugins/parallax-slider/css/parallax-slider.css');
    Yii::app()->getClientScript()->registerCssFile($mainAssets . '/css/theme-skins/dark.css');
    Yii::app()->getClientScript()->registerCssFile($mainAssets . '/css/custom.css');

    // yupe JS
    Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/js/blog.js');
    Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/js/bootstrap-notify.js');
    Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/js/jquery.li-translit.js');


    Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/js/app.js');
    Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/plugins/back-to-top.js');
    Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/plugins/smoothScroll.js');
    Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/plugins/parallax-slider/js/modernizr.js');
    Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/plugins/parallax-slider/js/jquery.cslider.js');
    Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/plugins/owl-carousel/owl-carousel/owl.carousel.js');
    Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/js/plugins/owl-carousel.js');
    Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/js/plugins/parallax-slider.js');
    Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/js/custom.js');
    //Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/js/app.js');
    ?>
    <script type="text/javascript">
        var yupeTokenName = '<?php echo Yii::app()->getRequest()->csrfTokenName;?>';
        var yupeToken = '<?php echo Yii::app()->getRequest()->csrfToken;?>';
    </script>
</head>
<body class="dark boxed-layout container">

<!-- .wrapper -->
<?php echo Chtml::tag('div',['class'=>'wrapper'])?>

<!--=== Header ===-->
<!--=== Header ===-->
<div class="header">
    <div class="container">
        <!-- Logo -->
        <a class="logo" href="/">
            <img src="<?=$mainAssets;?>/images/logo.png" alt="Logo">
        </a>
        <!-- End Logo -->

        <!-- Topbar -->
        <div class="topbar">
            <ul class="loginbar pull-right">
                <li class="hoverSelector">
                    <i class="fa fa-globe"></i>
                    <a>Languages</a>
                    <ul class="languages hoverSelectorBlock">
                        <?php /*echo CVarDumper::dump($this->yupe->getLanguageSelectorArray());*/?>
                        <li class="active">
                            <a href="#">English <i class="fa fa-check"></i></a>
                        </li>
                        <li><a href="#">Ukrainian</a></li>
                        <li><a href="#">Russian</a></li>
                        <li><a href="#">Chinese</a></li>
                    </ul>
                </li>
                <li class="topbar-devider"></li>
                <li><a href="page_faq.html">Help</a></li>
                <li class="topbar-devider"></li>
                <li><a href="page_login.html">Login</a></li>
            </ul>
        </div>
        <!-- End Topbar -->

        <!-- Toggle get grouped for better mobile display -->
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="fa fa-bars"></span>
        </button>
        <!-- End Toggle -->
    </div><!--/end container-->

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse mega-menu navbar-responsive-collapse">
        <div class="container">
            <?php if (Yii::app()->hasModule('menu')): ?>
                <?php $this->widget(
                    'application.modules.menu.widgets.MenuWidget', [
                        'name' => 'top-menu',
                        'layout' => 'top-menu'
                    ]
                ); ?>
            <?php endif; ?>

        </div><!--/end container-->
    </div><!--/navbar-collapse-->
</div>
<!--=== End Header ===-->


    <?php /*echo Chtml::tag('div',['class'=>'header-v6 header-classic-white header-sticky'])*/?><!--
        <?php /*echo Chtml::tag('div',['class'=>'navbar mega-menu', 'role'=>'navigation'])*/?>
            --><?php /*echo Chtml::tag('div',['class'=>'container'])*/?>


                <?php /*if (Yii::app()->hasModule('menu')): */?><!--
                    <?php /*$this->widget(
                        'application.modules.menu.widgets.MenuWidget', [
                            'name' => 'top-menu',
                        ]
                    ); */?>
                --><?php /*endif; */?>

            <?php /*echo Chtml::closeTag('div')*/?><!--
        <?php /*echo Chtml::closeTag('div')*/?>
    --><?php /*echo Chtml::closeTag('div')*/?>
<!--=== End Header ===-->

<?php if(Yii::app()->getRequest()->requestUri == '/'){?>
    <!--=== Slider ===-->
    <?php $this->widget(
        "application.modules.contentblock.widgets.ContentBlockWidget",
        array("code" => "banner-na-glavnoy"));
    ?>
    <!--=== End Slider ===-->
<?php }?>



<!-- breadcrumbs -->
<?php /*$this->widget(
    'bootstrap.widgets.TbBreadcrumbs',
    [
        'links' => $this->breadcrumbs,
    ]
);*/?>
<!-- end breadcrumbs -->

<!-- content -->

<?php echo $content; ?>

<!-- end content -->

<!--=== Footer Version 1 ===-->
<div class="footer-v1">
    <div class="footer">
        <div class="container">
            <div class="row">
                <!-- About -->
                <div class="col-md-3 md-margin-bottom-40">
                    <a href="index.html"><img id="logo-footer" class="footer-logo" src="<?php echo $mainAssets?>/img/logo2-default.png" alt=""></a>
                    <p>Текст о Проекте.</p>
                    <p>МОжно написать еще что то для большей информативности.</p>
                </div><!--/col-md-3-->
                <!-- End About -->

                <!-- Latest -->
                <div class="col-md-3 md-margin-bottom-40">
                    <div class="posts">
                        <div class="headline"><h2>Последние посты</h2></div>
                        <ul class="list-unstyled latest-list">
                            <li>
                                <a href="#">Incredible content</a>
                                <small>May 8, 2014</small>
                            </li>
                            <li>
                                <a href="#">Best shoots</a>
                                <small>June 23, 2014</small>
                            </li>
                            <li>
                                <a href="#">New Terms and Conditions</a>
                                <small>September 15, 2014</small>
                            </li>
                        </ul>
                    </div>
                </div><!--/col-md-3-->
                <!-- End Latest -->

                <!-- Link List -->
                <div class="col-md-3 md-margin-bottom-40">
                    <div class="headline"><h2>Полезные ссылки</h2></div>
                    <ul class="list-unstyled link-list">
                        <li><a href="#">О проекте</a><i class="fa fa-angle-right"></i></li>
                        <li><a href="#">Лучшее</a><i class="fa fa-angle-right"></i></li>
                        <li><a href="#">Последнее</a><i class="fa fa-angle-right"></i></li>
                        <li><a href="#">Сообщество</a><i class="fa fa-angle-right"></i></li>
                        <li><a href="#">Напишите нам</a><i class="fa fa-angle-right"></i></li>
                    </ul>
                </div><!--/col-md-3-->
                <!-- End Link List -->

                <!-- Address -->
                <div class="col-md-3 map-img md-margin-bottom-40">
                    <div class="headline"><h2>Обратная связь</h2></div>
                    <address class="md-margin-bottom-40">
                        Адрес компании для корреспонденции <br />
                        Россия, Санкт-Петербург <br />
                        Телефон: 800 123 3456 <br />
                        Email: <a href="mailto:info@anileria.com" class="">info@anileria.com</a>
                    </address>
                </div><!--/col-md-3-->
                <!-- End Address -->
            </div>
        </div>
    </div><!--/footer-->

    <div class="copyright">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <p>
                        <?php echo date("Y");?> &copy; All Rights Reserved.
                        <a href="#">Privacy Policy</a> | <a href="#">Terms of Service</a>
                    </p>
                </div>

                <!-- Social Links -->
                <div class="col-md-6">
                    <ul class="footer-socials list-inline">
                        <li>
                            <a href="#" class="tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Facebook">
                                <i class="fa fa-facebook"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Skype">
                                <i class="fa fa-skype"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Google Plus">
                                <i class="fa fa-google-plus"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Linkedin">
                                <i class="fa fa-linkedin"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Pinterest">
                                <i class="fa fa-pinterest"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Twitter">
                                <i class="fa fa-twitter"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Dribbble">
                                <i class="fa fa-dribbble"></i>
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- End Social Links -->
            </div>
        </div>
    </div><!--/copyright-->
</div>
<!--=== End Footer Version 1 ===-->


<?php echo Chtml::closeTag('div')?>
<!-- /.wrapper -->

<script>
    jQuery(document).ready(function() {
        App.init();
        OwlCarousel.initOwlCarousel();
        ParallaxSlider.initParallaxSlider();
    });
</script>
<!--[if lt IE 9]>
<script src="<?php echo $mainAssets?>/plugins/respond.js"></script>
<script src="<?php echo $mainAssets?>/plugins/html5shiv.js"></script>
<script src="<?php echo $mainAssets?>/plugins/placeholder-IE-fixes.js"></script>
<![endif]-->
</body>
</html>